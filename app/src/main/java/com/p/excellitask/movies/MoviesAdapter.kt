package com.p.excellitask.movies

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.p.excellitask.R
import com.p.excellitask.databinding.ItemMoviesBinding
import com.p.excellitask.loadLogo
import com.p.excellitask.movies.model.Search


/**
 * Created by Hyma on 3/4/21.
 */

class MoviesAdapter(
    context: Context,
    private val moviesList: List<Search>
) :
    RecyclerView.Adapter<MoviesAdapter.DashboardViewHolder>() {
    private val inflater = LayoutInflater.from(context)

    inner class DashboardViewHolder(val binding: ItemMoviesBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun setData(movie: Search) {
            binding.movie = movie
            binding.llItem.setOnClickListener(this)
        }

        override fun onClick(v: View) {
//            listener.onItemClick(v, adapterPosition, moviesList[adapterPosition])
//            Navigation.createNavigateOnClickListener(R.id.action_details)
            val bundle = Bundle()
            bundle.putString("movieName", moviesList[adapterPosition].title)
            Navigation.findNavController(v).navigate(R.id.detailFragment,bundle)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewHolder {
        return DashboardViewHolder(ItemMoviesBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        holder.binding.ivProvider.loadLogo(moviesList[position].poster, 10)
        holder.setData(moviesList[position])
    }
}