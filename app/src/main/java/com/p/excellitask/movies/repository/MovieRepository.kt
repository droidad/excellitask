package com.p.excellitask.movies.repository

import com.p.excellitask.webservice.IApiService
import com.p.excellitask.webservice.RetrofitService

/**
 * Created by Hyma on 4/4/21.
 */
class MovieRepository {

    private var client = RetrofitService.createService(IApiService::class.java)

    suspend fun getMovies(search: String) = client.getMovies(search)

    suspend fun getMovieDetails(movieName: String) = client.getMovieDetails(movieName)
}