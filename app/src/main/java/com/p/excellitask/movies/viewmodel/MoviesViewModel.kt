package com.p.excellitask.movies.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.p.excellitask.app.AppResult
import com.p.excellitask.base.BaseViewModel
import com.p.excellitask.movies.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Hyma on 4/4/21.
 */
class MoviesViewModel : BaseViewModel() {

    private val repository = MovieRepository()
    val moviesLiveData = MutableLiveData<AppResult<Any>>()

    val movieDetailsLiveData = MutableLiveData<AppResult<Any>>()

    fun getMovies(search: String) {
        viewModelScope.launch(Dispatchers.IO) {
            handleRequest(moviesLiveData) {
                repository.getMovies(search)
            }
        }
    }

    fun getMovieDetails(movieName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            handleRequest(movieDetailsLiveData) {
                repository.getMovieDetails(movieName)
            }
        }
    }
}