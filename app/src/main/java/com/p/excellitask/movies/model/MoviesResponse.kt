package com.p.excellitask.movies.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Hyma on 3/4/21.
 */

data class MoviesResponse(
    @SerializedName("Search")
    val search: List<Search>
)

data class Search(
    @SerializedName("Title")
    val title: String,
    @SerializedName("Year")
    val year: String,
    @SerializedName("imdbID")
    val imdbID: String,
    @SerializedName("Type")
    val type: String,
    @SerializedName("Poster")
    val poster: String
)