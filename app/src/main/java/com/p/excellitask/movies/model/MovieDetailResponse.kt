package com.p.excellitask.movies.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Hyma on 4/4/21.
 */
data class MovieDetailResponse(
    @SerializedName("Title")
    val title: String,
    @SerializedName("Plot")
    val plot: String,
    @SerializedName("Poster")
    val poster: String,
    @SerializedName("imdbRating")
    val imdbRating: String,
    @SerializedName("imdbVotes")
    val imdbVotes: String
)