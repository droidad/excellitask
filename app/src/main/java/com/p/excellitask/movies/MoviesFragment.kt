package com.p.excellitask.movies

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.p.excellitask.R
import com.p.excellitask.app.AppResult
import com.p.excellitask.base.BaseFragment
import com.p.excellitask.databinding.FragmentMoviesBinding
import com.p.excellitask.movies.model.MoviesResponse
import com.p.excellitask.movies.viewmodel.MoviesViewModel
import com.p.excellitask.showToast

/**
 * Created by Hyma on 3/4/21.
 */
class MoviesFragment : BaseFragment<FragmentMoviesBinding>() {

    override val layout = R.layout.fragment_movies

    private val viewModel: MoviesViewModel by viewModels()

    private lateinit var homeActivity: HomeActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = context as HomeActivity
    }

    override fun doInitialization() {

    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeActivity.showCommonHeader()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        viewModel.moviesLiveData.observe(viewLifecycleOwner, Observer {
            baseActivity.hideProgress()
            when (it) {
                is AppResult.Success -> {
                    val response = it.output as MoviesResponse
                    binding?.rvMovies?.adapter =
                        MoviesAdapter(baseActivity, response.search)
                    binding?.rvMovies?.addItemDecoration(
                        DividerItemDecoration(
                            baseActivity,
                            DividerItemDecoration.VERTICAL
                        )
                    )
                }
                is AppResult.Error -> {
                    baseActivity.showToast(it.error.error.message)
                }
                else -> {
                    baseActivity.showProgress()
                }
            }
        })
        viewModel.getMovies("friends")
    }
}