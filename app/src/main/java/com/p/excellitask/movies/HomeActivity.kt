package com.p.excellitask.movies

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.p.excellitask.R
import com.p.excellitask.base.BaseActivity
import com.p.excellitask.databinding.ActivityHomeBinding

/**
 * Created by Hyma on 3/4/21.
 */
class HomeActivity : BaseActivity() {

    lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        initHeader()
    }


    override fun getProgressBar(): View? {
        return binding.rlProgress
    }

    override fun getBackButton(): View? {
        return binding.incHeader.ivBack
    }

    override fun getHeader(): View? {
        return binding.incHeader.root
    }

    override fun getHeading(): View? {
        return binding.incHeader.tvTitle
    }

    override fun getMenuButton(): View? {
        return binding.incHeader.ivMenu
    }

    fun showCommonHeader() {
        binding.incHeader.ivMenu.visibility = View.VISIBLE
        binding.incHeader.ivBack.visibility = View.VISIBLE
        binding.incHeader.tvTitle.visibility = View.VISIBLE
        binding.incHeader.tvTitle.text = getString(R.string.movies)
    }
}