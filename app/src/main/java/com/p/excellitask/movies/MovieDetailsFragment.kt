package com.p.excellitask.movies

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.p.excellitask.R
import com.p.excellitask.app.AppResult
import com.p.excellitask.base.BaseFragment
import com.p.excellitask.databinding.FragmentMoviesDetailsBinding
import com.p.excellitask.loadBlurImage
import com.p.excellitask.loadLogo
import com.p.excellitask.movies.model.MovieDetailResponse
import com.p.excellitask.movies.viewmodel.MoviesViewModel
import com.p.excellitask.showToast


/**
 * Created by Hyma on 3/4/21.
 */
class MovieDetailsFragment : BaseFragment<FragmentMoviesDetailsBinding>() {

    override val layout = R.layout.fragment_movies_details

    private val viewModel: MoviesViewModel by viewModels()

    override fun doInitialization() {
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        baseActivity.getHeader()?.visibility = View.GONE

        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        val movieName: String = arguments?.getString("movieName", "friends")!!

        viewModel.movieDetailsLiveData.observe(viewLifecycleOwner, Observer {
            baseActivity.hideProgress()
            when (it) {
                is AppResult.Success -> {
                    val response = it.output as MovieDetailResponse

                    binding?.movie = response
                    binding?.ivMovie?.loadLogo(response.poster, 10)
                    binding?.ivBackPic?.loadBlurImage(response.poster, 1)
                }
                is AppResult.Error -> {
                    baseActivity.showToast(it.error.error.message)
                }
                else -> {
                    baseActivity.showProgress()
                }
            }
        })
        viewModel.getMovieDetails(movieName)
    }
}
