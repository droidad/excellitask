package com.p.excellitask.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 * Created by Hyma on 3/4/21.
 */
abstract class BaseFragment<T : ViewDataBinding> : Fragment() {
    internal var binding: T? = null

    @get:LayoutRes
    internal abstract val layout: Int

    protected lateinit var baseActivity: BaseActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, layout, container, false)
            doInitialization()
        } else {
            container?.removeView(binding?.root)
        }
        return binding?.root
    }

    abstract fun doInitialization()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity = context as BaseActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseActivity.getHeader()?.visibility = View.VISIBLE
        baseActivity.hideBackButton()
    }

    open fun onBackPressed(): Boolean {
        return false
    }
}