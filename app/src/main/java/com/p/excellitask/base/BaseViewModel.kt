package com.p.excellitask.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.p.excellitask.app.AppResult
import com.p.excellitask.asError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Created by Hyma on 4/4/21.
 */
open class BaseViewModel : ViewModel() {

    suspend fun <T : Any> handleRequest(
        liveData: MutableLiveData<AppResult<Any>>,
        requestFunc: suspend () -> T
    ) {
        try {
            withContext(Dispatchers.Main) {
                liveData.value = AppResult.Loading(true)
            }
            withContext(Dispatchers.Main) {
                liveData.value = AppResult.Success(requestFunc.invoke())
            }
        } catch (ex: Exception) {
            ex.asError().also {
                withContext(Dispatchers.Main) {
                    liveData.value = AppResult.Error(it)
                }
            }
        }
    }
}