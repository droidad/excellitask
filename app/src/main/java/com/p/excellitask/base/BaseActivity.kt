package com.p.excellitask.base

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * Created by Hyma on 3/4/21.
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract fun getProgressBar(): View?
    abstract fun getBackButton(): View?
    abstract fun getHeader(): View?
    abstract fun getHeading(): View?
    abstract fun getMenuButton(): View?

    open fun showProgress() {
        getProgressBar()?.visibility = View.VISIBLE
    }

    open fun hideProgress() {
        getProgressBar()?.visibility = View.GONE
    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.fragments.lastOrNull()
    }

    open fun hideBackButton() {
        getBackButton()?.visibility = View.GONE
    }

    protected open fun initHeader() {
        getBackButton()?.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if (getProgressBar()?.isShown == true) {
            return
        }
        val currentFragment = getCurrentFragment()
        if (currentFragment is BaseFragment<*> && currentFragment.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }
}