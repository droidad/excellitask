package com.p.excellitask.app

import com.google.gson.annotations.SerializedName

/**
 * Created by Hyma on 4/4/21.
 */
data class ErrorResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("error")
    val error: APIError
)

data class APIError(
    @SerializedName("code")
    val code: Int,
    @SerializedName("message")
    var message: String
)