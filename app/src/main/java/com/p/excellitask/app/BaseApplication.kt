package com.p.excellitask.app

import android.app.Application

/**
 * Created by Hyma on 3/4/21.
 */
class BaseApplication : Application() {
    companion object {
        lateinit var mInstance: BaseApplication
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
    }
}