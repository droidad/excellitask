package com.p.excellitask.app

/**
 * Created by Hyma on 4/4/21.
 */
sealed class AppResult<out T : Any> {
    data class Success<out T : Any>(val output: T) : AppResult<T>()
    data class Error(val error: ErrorResponse) : AppResult<ErrorResponse>()
    data class Loading(val boolean: Boolean) : AppResult<Nothing>()
}