package com.p.excellitask

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.p.excellitask.app.APIError
import com.p.excellitask.app.ErrorResponse
import jp.wasabeef.glide.transformations.BlurTransformation
import retrofit2.HttpException
import java.net.ConnectException


/**
 * Created by Hyma on 4/4/21.
 */


fun Context.showToast(message: CharSequence?, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun HttpException.asError(): ErrorResponse {
    return try {
        val errorBody = response()?.errorBody()?.charStream()
        val jsonReader: JsonReader = Gson().newJsonReader(errorBody)
        val errorResponse = Gson().getAdapter(ErrorResponse::class.java).read(jsonReader)
        errorResponse
    } catch (ex: Exception) {
        ErrorResponse(
            false, APIError(500, "Something went wrong")
        )
    }
}

fun Exception.asError(): ErrorResponse {
    this.printStackTrace()
    return when (this) {
        is HttpException -> {
            this.asError()
        }
        is ConnectException -> {
            ErrorResponse(
                false, APIError(1000, "Unable to connect to server")
            )
        }
        else -> {
            ErrorResponse(
                false, APIError(500, "Something went wrong")
            )
        }
    }
}

internal fun ImageView.loadLogo(url: String, radius: Int = 0) {
    val finalRadius = (context.resources.displayMetrics.density * radius).toInt()
    Glide.with(this)
        .asBitmap()
        .load(url)
        .apply(
            RequestOptions()
                .transform(CenterCrop(), RoundedCorners(finalRadius))
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .fallback(R.drawable.ic_launcher_foreground)
        ).into(this)
}

internal fun <T> ImageView.loadBlurImage(url: T, corners: Int = 0) {
    val finalCorner = resources.displayMetrics.density * corners
    Glide.with(this).asBitmap().load(url)
//        .centerCrop()
        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
        .into(this)
}
