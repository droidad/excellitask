package com.p.excellitask.webservice

import com.p.excellitask.movies.model.MovieDetailResponse
import com.p.excellitask.movies.model.MoviesResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Hyma on 3/4/21.
 */
interface IApiService {

    @GET("?apikey=5cb4645b")
    suspend fun getMovies(@Query("s") search: String): MoviesResponse

    @GET("?apikey=5cb4645b")
    suspend fun getMovieDetails(@Query("t") movieName: String): MovieDetailResponse

}